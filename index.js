const express = require("express");
const app = express();
const port = 8000;
const oracledb = require("oracledb");

const database = require("./database_connection.js");

async function getData(connection) {
  try {
    let sql, binds, options, result;

    sql = `SELECT * FROM CATEGORIES`;

    binds = {};

    options = {
      outFormat: oracledb.OUT_FORMAT_OBJECT,
    };

    result = await connection.execute(sql, binds, options);

    return result.rows;
  } catch (err) {
    console.error(err);
  }
}

// GET - Consulta
app.get("/funcionario", async (request, response) => {
  console.log("Acessando o recurso funcionário");

  const conn = await database.connection();
  
  const responseDatabase = await getData(conn);

  conn.close()

  response.json(responseDatabase);
});

// GET - Consultaa
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json())
app.post("/funcionario/gravar", (request, response) => {
  let valores = request.body
  console.log("Acessando o recurso funcionário");
  console.log("body: " + request);
  console.log("req.body: " + request.body);
  console.log("valores: " + valores);
  console.log("Sobrenome: " + valores.sobrenome);
  console.log("Idade: "+ valores.idade);
  response.send("sucesso");
});





// Habilitando o servidor
app.listen(port, async (req, res) => {
  console.log("Projeto executando na porta " + port);
});
